extern crate rand;

use rand::prelude::*;
use std::collections::HashMap;

fn modulo (val: i64, base: i64) -> i64 {
    ((val % base) + base) % base
}

struct Gene {
    value: u64,
    length: u8
}

struct Entity {
    health: u8,
    gene: Gene,
    level: u8
    //row: u32,
    //col: u32,
    //chr: char
}

fn get_rand (max: i64) -> i64 {
    thread_rng().gen_range(0, max)
}

fn spawn_entity (lvl: u8) -> Entity {
    Entity {
        health: 20,
        level: lvl,
        gene: Gene {
            value: random(),
            length: get_rand (21) as u8
        }
    }
}

fn choose_start_coords (world_size: i64) -> Box <Fn () -> i64> {
    Box::new (move || get_rand(world_size))
}
/*
struct World {
    main_vec: Vec<&Option<Gene>>,
    width: u64,
    height: u64,

}
*/

/*
fn create_entity (l: u8, c: char) -> Entity {
    //screen_vec[1] = &Some (Gene {value: 83, length: 5});
    Entity {gene: Gene {
        value: rand::random::<u64>(),
        length: thread_rng().gen_range(0, 10)
    }, level: l, chr: c,}
}
*/

fn _get_triplet (e: &Entity, i: u64) -> u64 {
    (e.gene.value >> ((i % (e.gene.length as u64)) * 3)) & 0b111
}

/*
fn read_screen (next: &Option<Gene>) -> char {
    match next {
        &Some(_) => '0',
        &None   => '.'
    }
}
*/

/*
fn show_screen (screen_vec: &Vec<u32>, ent_map: &HashMap<u32, Entity>) {
    for next in screen_vec.iter() {
        if *next == 0 {
            print! (".");
        } else {
            match ent_map.get (next) {
                None => print! ("."),
                Some (x) => print! ("{}", x.level)
            }
        }
    }
    println!("");
}
*/

fn show_screen (width: i64) ->
        Box <Fn (&Vec<u32>, &HashMap<u32, Entity>)> {
    Box::new (move |screen_vec, ent_map| {
        for next in screen_vec.iter() {
            //print! ("{}", next);
            if *next == 0 {
                print! (".");
            } else {
                match ent_map.get (next) {
                    None => print! ("!"),
                    Some (x) => print! ("{}", x.level)
                }
            }
        }
        println!("");
    })
}

fn get_move_right (width: i64, height: i64) -> Box <Fn (i64) -> i64> {
    Box::new (move |current| current - (current % width) + (current + 1) % width)
}

fn get_move_left (width: i64, height: i64) -> Box <Fn (i64) -> i64> {
    Box::new (move |current| (current % width) + modulo (current - 1, width))
}

fn get_move_down (width: i64, height: i64) -> Box <Fn (i64) -> i64> {
    Box::new (move |current| (current + width) % (width * height))
}

fn get_move_up (width: i64, height: i64) -> Box <Fn (i64) -> i64> {
    Box::new (move |current| modulo (current - width, width * height))
}

fn get_move_elem (
    move_up: Box <Fn (i64) -> i64>,
    move_down: Box <Fn (i64) -> i64>,
    move_left: Box <Fn (i64) -> i64>,
    move_right: Box <Fn (i64) -> i64>
) -> Box <Fn (i64, i64) -> i64> {
    Box::new (move |current, direction| {
        match direction {
            0 => move_up (current),
            1 => move_up (move_right (current)),
            2 => move_right (current),
            3 => move_down (move_right (current)),
            4 => move_down (current),
            5 => move_down (move_left (current)),
            6 => move_left (current),
            7 => move_up (move_left (current)),
            _ => current
        }
    })
}

fn create_world (world_width: i64, world_height: i64, start_pop: u8) -> bool {

    let mut turn: u64 = 0;
    let mut ent_counter: u32 = (start_pop) as u32;
    let world_size: i64 = world_width * world_height;

    let show_screen_inner = show_screen (world_width);

    let move_right: Box <Fn (i64) -> i64> = get_move_right (
        world_width, world_height);
    let move_left: Box <Fn (i64) -> i64> = get_move_left (
        world_width, world_height);
    let move_down: Box <Fn (i64) -> i64> = get_move_down (
        world_width, world_height);
    let move_up: Box <Fn (i64) -> i64> = get_move_up (
        world_width, world_height);
    let move_elem = get_move_elem (move_up, move_down, move_left, move_right);

    let mut world_vec: Vec<u32> = vec![0; (world_size) as usize];
    let mut entity_map: HashMap <u32, Entity> = HashMap::new();

    for next in 1..(ent_counter + 1) {
        let spawn_point: i64 = get_rand (world_size);
        if world_vec [spawn_point as usize] == 0 {
            /*
            println! ("Creating entity at position {} for index {}",
                spawn_point, next);
            */
            entity_map.insert (next, spawn_entity (1));
            world_vec [spawn_point as usize] = next;
        }
        //show_screen_inner (&world_vec, &entity_map);
    }

    //entity_map.insert (1, spawn_entity (1));
    //entity_map.insert (2, spawn_entity (1));
    //entity_map.insert (3, spawn_entity (1));



    /*
    for next in world_vec.iter() {
        if *next == 0 {
            print! (".");
        } else {
            let ent: Option<&Entity> = entity_map.get (next);

            match ent {
                None => print! ("."),
                Some (x) => print! ("{:?}", x.level)
            }
        }
    }
    */

    show_screen_inner (&world_vec, &entity_map);

    //world_vec.swap (0, (move_elem (0, 5)) as usize);
    //world_vec.swap (2, (move_elem (2, 0)) as usize);

    //show_screen_inner (&world_vec, &entity_map);

    /*
    for next in screen_vec.iter() {
        print!("{}", read_screen (next));
    }
    println!("");

    //screen_vec[0] = empty_gene;
    //screen_vec[2] = empty_gene;
    screen_vec[0] = &None;
    screen_vec[2] = &None;

    for next in screen_vec.iter() {
        print!("{}", read_screen (next));
    }
    println!("");
    */

    true

}

fn main(){

    let screen_cols: i64 = 6;
    let screen_rows: i64 = 5;
    let start_pop: u8 = 6;

    create_world (screen_cols, screen_rows, start_pop);

    /*
    for x in 0..(screen_cols * screen_rows){
        println!("{}", x);
    }
    */

    /*
    let ent1 = spawn_entity (2);
    let ent2 = spawn_entity (2);
    let ent3 = spawn_entity (2);
    let ent4 = spawn_entity (2);
    let ent5 = spawn_entity (2);
    let ent6 = spawn_entity (2);

    println! ("{}", ent1.gene.length);
    println! ("{}", ent2.gene.length);
    println! ("{}", ent3.gene.length);
    println! ("{}", ent4.gene.length);
    println! ("{}", ent5.gene.length);
    println! ("{}", ent6.gene.length);
    */

}

#[test]
fn test_stuff() {
}
