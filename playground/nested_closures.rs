fn main() {

    let add2 = |x: u64| x + 2;
    let add5 = |x: u64| add2 (x) + 3;

    println! ({}, add5(7));

}
